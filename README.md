# Terraform Google Cloud Log Sink for namespaces and route log to each individual projects

This Terraform code is tailored for GKE (Google Kubernetes Engine) and Anthos clusters. It provisions a Google Cloud Logging bucket, creates a sink for collecting logs from a source project related to the cluster, and grants appropriate permissions for the logging bucket in the destination project.


## Usage

Replace the variables in the variables.tf file to match your actual environment

```hcl
PROJECT_ID
GKECLUSTER
MAIN_PROJECT
TENANT_PROJECTS
LOCATION
RETENTION_DAYS
```

If you have multiple namespaces need to route the log, edit the TENANT_PROJECTS list.
For example:

```hcl
variable "TENANT_PROJECTS" {
  default = ["izekc3", "izekc1"]
}
```


## Requirements

- Terraform v1.6 or newer
- A Google Cloud Platform account and project
- Appropriate IAM permissions to create and manage GCP Logging resources
- A GKE or Anthos cluster setup

## Inputs

| Name                | Description                          | Type   | Default | Required |
|---------------------|--------------------------------------|--------|---------|:--------:|
| `PROJECT_ID`        | The ID of the Main GCP project.      | string | n/a     | yes      |
| `TENANT_PROJECTS`.  | The ID of the destination GCP project where logs will be stored. | string | n/a | yes |
| `CLUSTER_NAME`      | The name of the GKE or Anthos cluster.  | list(string) | n/a     | yes      |
| `LOCATION`          | The location of log bucket.  | string | n/a    | no      |
| `RETENTION_DAYS`    | Log retention on the destination bucket.  | number | 90    | no      |
| `multiple_clusters_per_sink`  | Option to use one sink for multiple cluster.  | bool | false    | no      |



## Outputs

| Name                  | Description                                               |
|-----------------------|-----------------------------------------------------------|
| `bucket_create`       | The objects of the created logging bucket.                |
| `log_sink_name`       | The objects of name of the created logging sink.          |
| `log_sink_destination`| The objects of name of the created logging sink.          |
| `log_view_name`       | The objects of name of the created logging sink.          |
| `log_view_id`       | The objects of name of the created logging sink.          |
| `logging_sa_account`     | The writer identity of the created logging sink.          |


## Importants

To avoid store duplicate data, you need to modify the _Default log sink to exclude the data that will route to other projects, otherwise, you will store duplicated data and charge twice.
From operation perspective, it is okay to keep the duplicate but with default 30 days retention, which will not add any extra charge.

## Add Cross-project environment monitoring feature to the code
Reference the link for more details https://cloud.google.com/composer/docs/terraform-cross-project-monitoring.

## Add Dashboard to monitor bucket ingestion rate on main project
- Total log ingested
- Top 10 Ingestion source project
- Log bucket Ingestion Rate by Project

## Copy Odd log over to newly create Project cloud log storage
Reference the link https://cloud.google.com/logging/docs/routing/copy-logs for more details.
You will be charged on log ingest

## License

This module is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Reference
https://github.com/telia-oss/terraform-gcp-gke-logsink-module/tree/main 