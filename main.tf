resource "random_string" "random_string" {
  length           = 8
  special          = false
}


resource "google_logging_project_bucket_config" "create_bucket" {
    for_each = toset(var.TENANT_PROJECTS)
    project    = each.key
    location   = var.LOCATION 
    retention_days = var.RETENTION_DAYS
    bucket_id = "gke-${each.key}-log-bucket-${random_string.random_string.id}"
    description = "Log bucket for ${each.key} namespace from ${var.MAIN_PROJECT}"
}


resource "google_logging_project_sink" "sink_create" {
  for_each = toset(var.TENANT_PROJECTS)
  name        = "gke-${each.key}-sink-${random_string.random_string.id}"

  # Can export to pubsub, cloud storage, bigquery, log bucket, or another project
  destination = "logging.googleapis.com/projects/${each.key}/locations/${var.LOCATION}/buckets/${google_logging_project_bucket_config.create_bucket[each.key].bucket_id}"

  # Log all WARN or higher severity messages relating to instances
  filter      = "resource.labels.namespace_name=${each.key} AND resource.labels.cluster_name=${var.CLUSTER_NAME}"

  # Use a unique writer (creates a unique service account used for writing)
  unique_writer_identity = true
  description = "Log sink to ${each.key} for ${each.key} namespace from ${var.MAIN_PROJECT}"
  project     = var.MAIN_PROJECT

  depends_on  = [google_logging_project_bucket_config.create_bucket]

}

resource "google_project_iam_binding" "logs-bucket-writer" {

  for_each = toset(var.TENANT_PROJECTS)
  project = each.key
  role    = "roles/logging.bucketWriter"

  depends_on  = [google_logging_project_sink.sink_create]

  members = [
    google_logging_project_sink.sink_create[each.key].writer_identity,
  ]
}

resource "google_logging_log_view" "logging_log_view" {
  for_each = toset(var.TENANT_PROJECTS)
  name        = "${google_logging_project_bucket_config.create_bucket[each.key].bucket_id}"
  bucket      = google_logging_project_bucket_config.create_bucket[each.key].bucket_id
  description = "Log view for the multi-tenancy log namespace ${each.key}"
  location    = var.LOCATION
  parent      = "projects/${each.key}"
  depends_on  = [google_project_iam_binding.logs-bucket-writer]
}


resource "google_monitoring_monitored_project" "projects_monitored" {
  for_each      = toset(var.TENANT_PROJECTS)
  metrics_scope = join("", ["locations/global/metricsScopes/", var.MAIN_PROJECT])
  name          = each.value
  depends_on = [google_logging_log_view.logging_log_view]
}


resource "google_monitoring_dashboard" "dashboard" {

  project    = var.MAIN_PROJECT
  dashboard_json = <<EOF
{
  "dashboardFilters": [],
  "displayName": "Log Bucket Ingestion Dashboard",
  "labels": {},
  "mosaicLayout": {
    "columns": 48,
    "tiles": [
      {
        "height": 16,
        "widget": {
          "timeSeriesTable": {
            "columnSettings": [
              {
                "column": "value",
                "visible": false
              }
            ],
            "dataSets": [
              {
                "minAlignmentPeriod": "60s",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "60s",
                      "crossSeriesReducer": "REDUCE_MEAN",
                      "groupByFields": [
                        "resource.label.\"project_id\""
                      ],
                      "perSeriesAligner": "ALIGN_RATE"
                    },
                    "filter": "metric.type=\"logging.googleapis.com/billing/log_bucket_bytes_ingested\" resource.type=\"global\""
                  }
                }
              }
            ],
            "metricVisualization": "NUMBER"
          },
          "title": "Log bucket bytes ingestion rate by project"
        },
        "width": 24,
        "xPos": 24,
        "yPos": 15
      },
      {
        "height": 16,
        "widget": {
          "timeSeriesTable": {
            "columnSettings": [
              {
                "column": "value",
                "visible": false
              }
            ],
            "dataSets": [
              {
                "minAlignmentPeriod": "60s",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "60s",
                      "crossSeriesReducer": "REDUCE_SUM",
                      "groupByFields": [
                        "resource.label.\"project_id\""
                      ],
                      "perSeriesAligner": "ALIGN_RATE"
                    },
                    "filter": "metric.type=\"logging.googleapis.com/billing/log_bucket_bytes_ingested\" resource.type=\"global\"",
                    "pickTimeSeriesFilter": {
                      "direction": "TOP",
                      "numTimeSeries": 30,
                      "rankingMethod": "METHOD_MEAN"
                    }
                  }
                }
              }
            ],
            "metricVisualization": "BAR"
          },
          "title": "Top 10 ingested "
        },
        "width": 24,
        "yPos": 15
      },
      {
        "height": 15,
        "widget": {
          "timeSeriesTable": {
            "columnSettings": [
              {
                "column": "value",
                "visible": false
              }
            ],
            "dataSets": [
              {
                "minAlignmentPeriod": "60s",
                "timeSeriesQuery": {
                  "outputFullDuration": true,
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "60s",
                      "crossSeriesReducer": "REDUCE_SUM",
                      "groupByFields": [
                        "resource.label.\"project_id\""
                      ],
                      "perSeriesAligner": "ALIGN_RATE"
                    },
                    "filter": "metric.type=\"logging.googleapis.com/billing/log_bucket_bytes_ingested\" resource.type=\"global\"",
                    "secondaryAggregation": {
                      "alignmentPeriod": "60s",
                      "crossSeriesReducer": "REDUCE_SUM",
                      "groupByFields": [
                        "resource.label.\"project_id\""
                      ],
                      "perSeriesAligner": "ALIGN_SUM"
                    }
                  }
                }
              }
            ],
            "metricVisualization": "NUMBER"
          },
          "title": "Total log ingested"
        },
        "width": 48
      },
      {
        "height": 16,
        "widget": {
          "title": "Log bucket Daily bytes ingested [SUM]",
          "xyChart": {
            "chartOptions": {
              "mode": "COLOR"
            },
            "dataSets": [
              {
                "minAlignmentPeriod": "86400s",
                "plotType": "LINE",
                "targetAxis": "Y1",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "86400s",
                      "crossSeriesReducer": "REDUCE_SUM",
                      "groupByFields": [
                        "resource.label.\"project_id\""
                      ],
                      "perSeriesAligner": "ALIGN_MEAN"
                    },
                    "filter": "metric.type=\"logging.googleapis.com/billing/log_bucket_monthly_bytes_ingested\" resource.type=\"global\""
                  }
                }
              }
            ],
            "thresholds": [],
            "yAxis": {
              "label": "",
              "scale": "LINEAR"
            }
          }
        },
        "width": 48,
        "yPos": 31
      },
      {
        "height": 13,
        "widget": {
          "timeSeriesTable": {
            "columnSettings": [
              {
                "column": "value",
                "visible": false
              }
            ],
            "dataSets": [
              {
                "minAlignmentPeriod": "60s",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "60s",
                      "crossSeriesReducer": "REDUCE_SUM",
                      "groupByFields": [
                        "resource.label.\"project_id\"",
                        "resource.label.\"name\""
                      ],
                      "perSeriesAligner": "ALIGN_RATE"
                    },
                    "filter": "metric.type=\"logging.googleapis.com/exports/byte_count\" resource.type=\"logging_sink\" resource.label.\"name\"!=monitoring.regex.full_match(\"_Required|_Default\")"
                  }
                }
              }
            ],
            "metricVisualization": "NUMBER"
          },
          "title": "Exported log bytes (filtered) [SUM]"
        },
        "width": 48,
        "yPos": 47
      }
    ]
  }
}

EOF
}




####
#echo hello >> /proc/1/fd/1
###

###
# https://cloud.google.com/logging/docs/routing/copy-logs
###

### 
# modify _Default to exclude duplicated logs
###