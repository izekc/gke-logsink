#variable "ORG_ID" {
#  type = string
#  default = "413529449729"
#}

variable "PROJECT_ID" {
  type = string
  default = "project-alset"
}

variable "CLUSTER_NAME" {
  type = string
  default = "izekc-demo1"
}

variable "MAIN_PROJECT" {
  type = string
  default = "project-alset"
}

#variable "TENANT_PROJECT" {
#  type = string
#  default = "izekc3"
#}

variable "TENANT_PROJECTS" {
  default = ["izekc3", "izekc1"]
}

variable "LOCATION" {
  description = "The location of the logging bucket."
  type = string
  default = "asia-east1"
}

variable "RETENTION_DAYS" {
  description = "Number of days to retain log entries."
  type        = number
  default     = 90
}

