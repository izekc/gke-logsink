#output "random_number" {
#  value = ["${random_string.random_string.result}"]
#}


output "bucket_create" {
  value = google_logging_project_bucket_config.create_bucket[*]
}


output "log_sink_name" {
  value = google_logging_project_sink.sink_create[*]
}

output "log_sink_destination" {
  value = google_logging_project_sink.sink_create[*]
}


output "log_view_name" {
  value = google_logging_log_view.logging_log_view[*]
}

output "log_view_id" {
  value = google_logging_log_view.logging_log_view[*]
}


#output "logging_sa_account" {
#  value = google_logging_project_sink.sink_create[*]
#}
